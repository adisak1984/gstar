#include "BluetoothSerial.h"

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

BluetoothSerial SerialBT;
int inChar;
String code;
int a, b, c, d;

void setup() {
  Serial.begin(115200);
  SerialBT.begin("ESP32test"); //Bluetooth device name
  Serial.println("The device started, now you can pair it with bluetooth!");
}

void loop() {
  if (Serial.available()) {
    //SerialBT.write(Serial.read());
    String myString = "";
    String * str = NULL;    // สร้างตัวแปร String Array ที่จะใช้เก็บ String ที่ตัดแบ่งแล้ว
    String inChar;
    inChar = Serial.read();
    ///myString += (char)inChar;

    //Serial.println(inChar);
    int count = inChar.td_split( " ", &str );
    code = String(str[0]);
    a = str[1].toInt();
    b = str[2].toInt();
    c = str[3].toInt();
    d = str[4].toInt();
    Serial.print(code);
    Serial.print(a);
    Serial.print(b);
    Serial.print(c);
    Serial.println(d);
    delay(20);
  }
}
