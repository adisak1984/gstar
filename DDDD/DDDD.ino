#include "BluetoothSerial.h" //BluetoothSerial
BluetoothSerial SerialBT;
#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

#define RXD2 16
#define TXD2 17
uint8_t ledR = 27;
uint8_t ledG = 26;
uint8_t ledB = 25;
uint8_t ledF = 33;

uint8_t ledArray[4] = {1, 2, 3, 4}; // three led channels
const boolean invert = false;

//uint32_t R, G, B, F;          // the Red Green and Blue color components
String data, dat1, dat2, dat3, dat4, dat5, dat6;
String Open, Close;
float Freq;
int Red, Green, Blue;

void setup() {
  Serial.begin(115200);
  Serial2.begin(115200, SERIAL_8N1, RXD2, TXD2);
  SerialBT.begin("ESP32test"); //Bluetooth device name
  Serial.println("The device started, now you can pair it with bluetooth!");

  ledcAttachPin(ledF, 1);
  ledcAttachPin(ledR, 2); // assign RGB led pins to channels
  ledcAttachPin(ledG, 3);
  ledcAttachPin(ledB, 4);

  ledcSetup(1, 12000, 8); // 12 kHz PWM, 8-bit resolution
  ledcSetup(2, 12000, 8);
  ledcSetup(3, 12000, 8);
  ledcSetup(4, 12000, 8);
}
void loop() {
  while (SerialBT.available() > 0)
  {
    data = SerialBT.readStringUntil('\n');//อ่านค่ามาเก็บที่ data
    dat1 = getValue(data, ' ', 0);
    dat2 = getValue(data, ' ', 1);
    dat3 = getValue(data, ' ', 2);
    dat4 = getValue(data, ' ', 3);
    dat5 = getValue(data, ' ', 4);
    dat6 = getValue(data, ' ', 5);

    Open = dat1.toInt();
    Freq = dat2.toInt();
    Red = dat3.toInt();
    Green = dat4.toInt();
    Blue = dat5.toInt();
    Close = dat6.toInt();

    if (dat1 == "#")  {

      Serial.print(Freq); Serial.print(" ");
      Serial.print(Red); Serial.print(" ");
      Serial.print(Green); Serial.print(" ");
      Serial.print(Blue); Serial.print(" ");
      Serial.print(Close); Serial.println("OK");//Serial.println();

      ledcSetup(4, Freq, 8); ledcWrite(4, 128);
      ledcWrite(1, Red);
      ledcWrite(2, Green);
      ledcWrite(3, Blue);
    }
    else {
      Serial.println(data); Serial.print("HOO");
      Serial2.println(data);
    }
  }
}

String getValue(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = {0, -1  };
  int maxIndex = data.length() - 1;
  for (int i = 0; i <= maxIndex && found <= index; i++) {
    if (data.charAt(i) == separator || i == maxIndex) {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i + 1 : i;
    }
  }
  return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}
