#include <SoftwareSerial.h>
SoftwareSerial mySerial(5, 4); // RX, TX

#include <PWM.h>
int rl = 3;
int gl = 9;
int bl = 10;
int fq = 2;
int32_t frequency = 20000 ; //frequency (in Hz)

String code;
int a, b, c, d;
int inChar;
void setup() {
  Serial.begin(9600);
  mySerial.begin(9600);

  InitTimersSafe();
  SetPinFrequencySafe(fq, frequency);//pin, fre
  SetPinFrequencySafe(rl, frequency);//pin, fre
  SetPinFrequencySafe(gl, frequency);//pin, fre
  SetPinFrequencySafe(bl, frequency);//pin, fre
  pwmWrite(fq, 0);
  pwmWrite(rl, 0);
  pwmWrite(gl, 0);
  pwmWrite(bl, 0);
  //digitalWrite(fq, LOW);
}

void loop() {
  if (mySerial.available())
  {
    String myString = "";
    String * str = NULL;    // สร้างตัวแปร String Array ที่จะใช้เก็บ String ที่ตัดแบ่งแล้ว
    String inChar;
    inChar = mySerial.readString();
    ///myString += (char)inChar;

    //Serial.println(inChar);
    int count = inChar.td_split( " ", &str );
    code = String(str[0]);
    a = str[1].toInt();
    b = str[2].toInt();
    c = str[3].toInt();
    d = str[4].toInt();
    Serial.print(code);
    Serial.print(a);
    Serial.print(b);
    Serial.print(c);
    Serial.println(d);

    if (code = "#") {
      Serial.println("OK");
    }
  }
  //t=(1/2)*1000
  pwmWrite(rl, b);
  pwmWrite(gl, c);
  pwmWrite(bl, d);
  digitalWrite(fq, HIGH);   
  delay(100);                      
  digitalWrite(fq, LOW);    
   delay(200);  
  

}//end
