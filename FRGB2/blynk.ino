#include <PWM.h>
int r = 3;
int g = 6;
int b = 10;
int fe = 9; //9               // the pin that the LED is attached to
int brightness = 0;         // how bright the LED is
int fadeAmount = 5;         // how many points to fade the LED by
int32_t frequency = 35; //frequency (in Hz)

void setup()
{
  //initialize all timers except for 0, to save time keeping functions
  InitTimersSafe();
  pinMode(r, OUTPUT);
  pinMode(g, OUTPUT);
  pinMode(b, OUTPUT);
  pinMode(fe, OUTPUT);
}

void loop()
{
  SetPinFrequencySafe(fe, 20);
  pwmWrite(fe, 128);

analogWrite(r, 255);
analogWrite(g, 255);
analogWrite(b, 255);
}
