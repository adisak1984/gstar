#include <Wire.h>
#include "SparkFunHTU21D.h"

HTU21D myHumidity0;
HTU21D myHumidity1;
HTU21D myHumidity2;
HTU21D myHumidity3;
#define TCAADDR 0x70

void tcaselect(uint8_t i) {
  if (i > 7) return;
  Wire.beginTransmission(TCAADDR);
  Wire.write(1 << i);
  Wire.endTransmission();
}
void setup()
{
  Serial.begin(115200);
  Serial.println("HTU21D Example!");
  tcaselect(0); myHumidity0.begin();
  tcaselect(1); myHumidity1.begin();
  tcaselect(2); myHumidity2.begin();
  tcaselect(4); myHumidity3.begin();
}

void loop()
{
  TempAndHumd4();
}
void TempAndHumd4()
{ tcaselect(0);
  float temp0 = myHumidity0.readTemperature();
  float humd0 = myHumidity0.readHumidity();
  Serial.print(" Temp0:"); Serial.print(temp0, 1); Serial.print("C");
  Serial.print(" Humd0:"); Serial.print(humd0, 1); Serial.println("%");
  tcaselect(1);
  float temp1 = myHumidity1.readTemperature();
  float humd1 = myHumidity1.readHumidity();
  Serial.print(" Temp1:"); Serial.print(temp1, 1); Serial.print("C");
  Serial.print(" Humd1:"); Serial.print(humd1, 1); Serial.println("%");
  tcaselect(2);
  float temp2 = myHumidity1.readTemperature();
  float humd2 = myHumidity1.readHumidity();
  Serial.print(" Temp2:"); Serial.print(temp2, 1); Serial.print("C");
  Serial.print(" Humd2:"); Serial.print(humd2, 1); Serial.println("%");
  tcaselect(4);
  float temp3 = myHumidity3.readTemperature();
  float humd3 = myHumidity3.readHumidity();
  Serial.print(" Temp3:"); Serial.print(temp3, 1); Serial.print("C");
  Serial.print(" Humd3:"); Serial.print(humd3, 1); Serial.println("%");
  float tempsum  = (temp0 + temp1 + temp2 + temp3) / 4; Serial.print(" tempsum:"); Serial.print(tempsum, 1); Serial.print("C");
  float humdsum = (humd0 + humd1 + humd2 + humd3) / 4; Serial.print(" humdsum:"); Serial.print(humdsum, 1); Serial.println("%");
  Serial.println();
}
