#define RXD2 16
#define TXD2 17
#include "DHT.h"
DHT dht(4, DHT22);
uint8_t FanPin = 13;
float h, t, f;
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>
BLEServer *pServer = NULL;
BLECharacteristic *pCharacteristic;
bool deviceConnected = false;
bool oldDeviceConnected = false;

//std::string rxValue; // Could also make this a global var to access it in loop()

#define SERVICE_UUID           "6E400001-B5A3-F393-E0A9-E50E24DCCA9E" // UART service UUID
#define CHARACTERISTIC_UUID_RX "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_TX "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"

class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
    };
    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
    }
};

class MyCallbacks: public BLECharacteristicCallbacks {

    void ledcAnalogWrite(uint8_t channel, uint32_t value, uint32_t valueMax = 255) {
      uint32_t duty = (32764 / valueMax) * min(value, valueMax);// calculate duty, 8191 from 2 ^ 13 - 1
      ledcWrite(channel, duty);// write duty to LEDC
    }

    void forwardToLaser(std::string value)
    {
      String lastText = "";

      for (int i = 0; i < value.length(); i++)
      {
        if ((i) % 2)
        {
          String text = lastText + value[i];

          char bufferData[text.length() + 1];

          text.toCharArray(bufferData, text.length() + 1);

          unsigned long result = strtoul(bufferData, NULL, 16);

          Serial2.write(result);
        }
        else
        {
          lastText = value[i];
        }
      }
    }

    void onWrite(BLECharacteristic *pCharacteristic) {
      std::string rxValue = pCharacteristic->getValue();
      String Code, End;
      int f1, f2, f3, f4, f5, r1, r2, r3, g1, g2, g3, b1, b2, b3, red, green, blue;
      float frq, f6, f7;

      uint8_t ledF = 14;
      uint8_t ledR = 27;
      uint8_t ledG = 26;
      uint8_t ledB = 25;
      uint8_t ledArray[4] = {1, 2, 3, 4}; // three led channels
      const boolean invert = true;
      uint32_t R, G, B, F;
      ledcSetup(1, 25000, 15);
      ledcSetup(2, 25000, 15);
      ledcSetup(3, 25000, 15);
      ledcSetup(4, 25000, 15);

      if (rxValue.length() > 0) {
        for (int i = 0; i < rxValue.length(); i++) {
          Serial.print(rxValue[i]); Code = rxValue[0]; End = rxValue[17];

        }
      
        if ((Code == "@") && (End == "/")) {
          //Serial.print(Code);Serial.print(f1);Serial.print(f2);
          f1 = (rxValue[1] - 48) * 10000;
          f2 = (rxValue[2] - 48) * 1000 ;
          f3 = (rxValue[3] - 48) * 100;
          f4 = (rxValue[4] - 48) * 10;
          f5 = (rxValue[5] - 48) * 1;
          f6 = (rxValue[6] - 48) * 0.1;
          f7 = (rxValue[7] - 48) * 0.01;
          r1 = (rxValue[8] - 48) * 100;
          r2 = (rxValue[9] - 48) * 10;
          r3 = (rxValue[10] - 48) * 1;
          g1 = (rxValue[11] - 48) * 100;
          g2 = (rxValue[12] - 48) * 10;
          g3 = (rxValue[13] - 48) * 1;
          b1 = (rxValue[14] - 48) * 100;
          b2 = (rxValue[15] - 48) * 10;
          b3 = (rxValue[16] - 48) * 1;

          frq = f1 + f2 + f3 + f4 + f5 + f6 + f7;
          red = r1 + r2 + r3;
          green = g1 + g2 + g3;
          blue = b1 + b2 + b3;
          Serial.print(frq);
          Serial.print(" ");
          Serial.print(red);
          Serial.print(" ");
          Serial.print(green);
          Serial.print(" ");
          Serial.println(blue);

          ledcAttachPin(ledF, 1);
          ledcAttachPin(ledR, 2);
          ledcAttachPin(ledG, 3);
          ledcAttachPin(ledB, 4);

          ledcSetup(1, frq, 15);
          ledcAnalogWrite(1, 128);
          ledcAnalogWrite(2, red);
          ledcAnalogWrite(3, green);
          ledcAnalogWrite(4, blue);
        }
        else
        {
          forwardToLaser(rxValue);
        }
      }
    }
};
//fan con
void ledcAnalogWrite(uint8_t channel, uint32_t value, uint32_t valueMax = 255) {
  uint32_t duty = (32764 / valueMax) * min(value, valueMax);// calculate duty, 8191 from 2 ^ 13 - 1
  ledcWrite(channel, duty);  // write duty to LEDC
}


void fancontrol() {
  h = dht.readHumidity();
  t = dht.readTemperature();
  f = dht.readTemperature(true);
  if (isnan(h) || isnan(t) || isnan(f)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    t  = 0;
    return;
  }
  Serial.print(t);
  if ( t < 23) {
    ledcAnalogWrite(5, 0); Serial.println("t0");
  }
  if (t >= 23 && t < 32) {
    ledcAnalogWrite(5, 20); Serial.println("t1");
  }
  if (t >= 32) {
    ledcAnalogWrite(5, 200); Serial.println("t3");
  } delay(1);
}
void setup() {
  Serial2.begin(115200, SERIAL_8N1, RXD2, TXD2);
  Serial.begin(115200);
  dht.begin();
  ledcSetup(5, 1000, 13);
  ledcAttachPin(FanPin, 5);

  // Create the BLE Device
  BLEDevice::init("ESP32 UART Test"); // Give it a name

  // Create the BLE Server
  BLEServer *pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic
  pCharacteristic = pService->createCharacteristic(
                      CHARACTERISTIC_UUID_TX,
                      BLECharacteristic::PROPERTY_NOTIFY
                    );

  pCharacteristic->addDescriptor(new BLE2902());

  BLECharacteristic *pCharacteristic = pService->createCharacteristic(
                                         CHARACTERISTIC_UUID_RX,
                                         BLECharacteristic::PROPERTY_WRITE
                                       );

  pCharacteristic->setCallbacks(new MyCallbacks());

  // Start the service
  pService->start();

  // Start advertising
  pServer->getAdvertising()->start();
  Serial.println("Waiting a client connection to notify...");
}

void loop() {
  // disconnecting
  if (!deviceConnected && oldDeviceConnected) {
    delay(500); // give the bluetooth stack the chance to get things ready
    pServer->startAdvertising(); // restart advertising
    Serial.println("start advertising");
    oldDeviceConnected = deviceConnected;
    fancontrol();
  }
  // connecting
  if (deviceConnected && !oldDeviceConnected) {
    // do stuff here on connecting
    oldDeviceConnected = deviceConnected;
  }
}
