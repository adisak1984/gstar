#include <SoftwareSerial.h>
SoftwareSerial mySerial(4, 5); // RX, TX

#include <PWM.h>
int rl = 6;
int gl = 9;
int bl = 10;
int fq = 3;
int32_t frequency = 2000 ; //frequency (in Hz)

String code;
int a, b, c, d;
int inChar;
void setup() {
  Serial.begin(115200);
  mySerial.begin(9600);
  InitTimersSafe();
  SetPinFrequencySafe(fq, frequency);//pin, fre
  SetPinFrequencySafe(rl, frequency);//pin, fre
  SetPinFrequencySafe(gl, frequency);//pin, fre
  SetPinFrequencySafe(bl, frequency);//pin, fre
  pwmWrite(fq, 0);
  pwmWrite(rl, 0);
  pwmWrite(gl, 0);
  pwmWrite(bl, 0);
}

void loop()
{
  if (mySerial.available())
  {
    String myString = "";
    String * str = NULL;    // สร้างตัวแปร String Array ที่จะใช้เก็บ String ที่ตัดแบ่งแล้ว
    String inChar;
    inChar = mySerial.readString();
    int count = inChar.td_split( " ", &str );
    code = String(str[0]);
    a = str[1].toInt();
    b = str[2].toInt();
    c = str[3].toInt();
    d = str[4].toInt();
    if (code == "%")
    {
      Serial.print(a);
      Serial.print(b);
      Serial.print(c);
      Serial.print(d); Serial.println("OK");
      SetPinFrequencySafe(fq, a);
      pwmWrite(fq, 128);
      analogWrite(rl, b);
      analogWrite(gl, c);
      analogWrite(bl, d);
    }
    else
    { Serial.print("HAHA");
      Serial.println(inChar);
    }
  }
}
